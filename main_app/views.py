import hashlib
import requests
import threading
import uuid
from .models import Request
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def submit(request):
    if request.method == 'POST':

        def background_task(r_id):
            try:
                def md5sum(url, blocksize=65536):
                    hash = hashlib.md5()
                    with requests.get(url) as f:
                        for block in f.iter_content(chunk_size=blocksize):
                            hash.update(block)
                    return hash.hexdigest()

                request_model = Request.objects.get(id=r_id)
                request_model.hash = md5sum(request_model.file_url)
                request_model.status = Request.STATUS_DONE
                request_model.save()
            except Exception:
                request_model = Request.objects.get(id=r_id)
                request_model.status = Request.STATUS_ERROR
                request_model.save()

            if request_model.status == Request.STATUS_DONE and request_model.email:
                send_mail(subject='MD5 service - your request has been processed',
                          message='Your request has been processed. ID: ' + request_model.id +
                                  ', \nfile url: ' + request_model.file_url +
                                  ', \nmd5: ' + request_model.hash,
                          from_email=settings.EMAIL_HOST_USER,
                          recipient_list=[request_model.email],
                          fail_silently=True)

        request_id = str(uuid.uuid1())
        file_url = request.POST.get('url')
        email = request.POST.get('email')
        model = Request.objects.create(id=request_id, status=Request.STATUS_WIP, file_url=file_url, email=email)
        json = {'id': request_id}

        if model:
            thread = threading.Thread(target=background_task, args=(request_id,))
            thread.start()
            return JsonResponse(json)
        else:
            # 500 internal error
            return JsonResponse(status=500, data={'error': 'Server error while creating request'})
    else:
        # 400 bad request
        return JsonResponse(status=400, data={'error': 'Bad request'})


def check(request):
    if request.method == 'GET':
        request_id = request.GET.get('id')
        if not request_id:
            # 400 bad request
            return JsonResponse(status=400, data={'error': 'Bad request'})
        try:
            model = Request.objects.get(id=request_id)
        except ObjectDoesNotExist:
            return JsonResponse(status=404, data={'status': 'not found'})
        if model.status == Request.STATUS_DONE:
            json = {'md5': model.hash, 'status': model.get_status, 'url': model.file_url}
        else:
            json = {'status': model.get_status}
        return JsonResponse(json)

    else:
        # 400 bad request
        return JsonResponse(status=400, data={'error': 'Bad request'})
