from django.db import models
from django.utils import timezone


class Request(models.Model):
    STATUS_ERROR, STATUS_WIP, STATUS_DONE = range(-1, 2)
    Status = (
        (STATUS_ERROR, 'error'),
        (STATUS_WIP, 'running'),
        (STATUS_DONE, 'done'),
    )
    statuses = dict(Status)

    id = models.CharField(primary_key=True, max_length=36)
    file_url = models.CharField(max_length=2048)
    hash = models.CharField(max_length=32, null=True)
    status = models.IntegerField(choices=Status, default=STATUS_WIP)
    email = models.CharField(max_length=256, null=True)
    created_date = models.DateTimeField(
            default=timezone.now)

    def __str__(self):
        return str(self.created_date)

    @property
    def get_status(self):
        return self.statuses.get(self.status)
