# MD5 service

md5_service is a Django-based service for calculating md5 hash of any file in the internet.

## Installation

1. Clone this repository: `git clone git@bitbucket.org:dreyk111/md5-service.git`.
2. `cd` into project folder.
3. Install [Python with Django (SQLite Database will be using)](https://docs.djangoproject.com/en/2.1/topics/install/#installing-official-release).
4. Install `requests`: `pip install requests`
5. run comand to make database: `python manage.py migrate`
6. edit `settings.py` to configure SMTP email sending:
```python
# SMTP email send configuration
EMAIL_HOST = 'YOUR EMAIL HOST'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'YOUR EMAIL USER'
EMAIL_HOST_PASSWORD = 'YOUR PASSWORD'
EMAIL_USE_TLS = True
```
6. run server: `python manage.py runserver`


## Usage

### Make new request

```bash
curl -X POST -d
"email=user@example.com&url=http://site.com/file.txt"
http://localhost:8000/submit 
```
`url` - required parameter, full path to file for calculating MD5 hash

`email` - optional parameter, if set, sends MD5 hash on this e-mail when done

Returns the `id` of your request in JSON format.
### Check result

```bash
curl -X GET http://localhost:8000/check?id=0e4fac17-f367-
4807-8c28-8a059a2f82ac
```
`id` - required parameter, your unique request identificator

Returns the `status` of your request in JSON format. If done, also returns calculated `md5` and file `url`.

## License
[MIT](https://choosealicense.com/licenses/mit/)